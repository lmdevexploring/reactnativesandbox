/**
 * @format
 */

import {AppRegistry} from 'react-native'
import {name as appName} from './app.json'

// Getting Started
// import App from './apps/App';
// AppRegistry.registerComponent(appName, () => App);

// Learn the basics - Hello World
// import HelloWorldApp from './apps/HelloWorldApp';
// AppRegistry.registerComponent(appName, () => HelloWorldApp);

// Props
// import PropsApp from './apps/PropsApp';
// AppRegistry.registerComponent(appName, () => PropsApp);

// State
// import StateApp from './apps/StateApp';
// AppRegistry.registerComponent(appName, () => StateApp);

// StyleApp
// import StateApp from './apps/StyleApp';
// AppRegistry.registerComponent(appName, () => StateApp);

// DimensionsApp
// import DimensionsApp from './apps/DimensionsApp';
// AppRegistry.registerComponent(appName, () => DimensionsApp);

// FlexboxApp
// import FlexboxApp from './apps/FlexboxApp';
// AppRegistry.registerComponent(appName, () => FlexboxApp);

// FlexboxApp2
// import FlexboxApp2 from './apps/FlexboxApp2';
// AppRegistry.registerComponent(appName, () => FlexboxApp2);

// FlexboxAppWrap
// import FlexboxAppWrap from './apps/FlexboxAppWrap';
// AppRegistry.registerComponent(appName, () => FlexboxAppWrap);

// FlexboxAppWrap
// import FlexboxAppPosition from './apps/FlexboxAppPosition';
// AppRegistry.registerComponent(appName, () => FlexboxAppPosition);

// TextInputApp
// import TextInputApp from './apps/TextInputApp';
// AppRegistry.registerComponent(appName, () => TextInputApp);

// TextInputApp
// import ButtonsApp from './apps/ButtonsApp';
// AppRegistry.registerComponent(appName, () => ButtonsApp);

//TouchablesApp
// import TouchablesApp from './apps/TouchablesApp';
// AppRegistry.registerComponent(appName, () => TouchablesApp);

//ScrollViewApp
// import ScrollViewApp from './apps/ScrollViewApp';
// AppRegistry.registerComponent(appName, () => ScrollViewApp);

//ListViewApp
// import ListViewApp from './apps/ListViewApp';
// AppRegistry.registerComponent(appName, () => ListViewApp);

// Networking: Fetch
// import Fetch from './apps/NetworkingSandbox/Fetch';
// AppRegistry.registerComponent(appName, () => Fetch);

// Networking: UMinhoEspacos
// import UMinhoEspacos from './apps/NetworkingSandbox/UMinhoEspacos';
// AppRegistry.registerComponent(appName, () => UMinhoEspacos);

// ReactNativeElementsSandbox
// import ReactNativeElementsSandbox from './apps/ReactNativeElementsSandbox/NativeElements';
// AppRegistry.registerComponent(appName, () => ReactNativeElementsSandbox);

// ReactNativeElementsSandbox2
import NativeElements from './apps/ReactNativeElementsSandbox2/NativeElements';
AppRegistry.registerComponent(appName, () => NativeElements);



