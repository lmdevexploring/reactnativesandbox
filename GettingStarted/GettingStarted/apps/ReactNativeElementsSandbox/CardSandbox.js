import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Card, ListItem, Button, Avatar, Icon, Text } from 'react-native-elements'

export class CardSandbox extends React.Component {

render()
{


    const users = [
        {
            name: 'Brynn',
            avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg'
        },

        {
            name: 'Joaquina',
            avatar: 'https://randomuser.me/api/portraits/women/2.jpg'
        },
    
        {
            name: 'Alberto',
            avatar: 'https://randomuser.me/api/portraits/men/27.jpg'
        },        
    
        {
            name: 'João',
            avatar: 'https://randomuser.me/api/portraits/men/17.jpg'
        },                
    
        {
            name: 'Albertina',
            avatar: 'https://randomuser.me/api/portraits/women/68.jpg'
        },    
   
    ]
   
    const styles = StyleSheet.create({
        image: {
         
        },
        user: {
            flex: 1,
            justifyContent: "flex-start",
            flexDirection: "row",
            padding: 10
        },
        name: {
            marginLeft: 20,
            fontSize: 15,
            alignContent: "space-around"
        },
      });

    return (
       <View >

           <Text h1>Cards</Text>

            {/* // implemented without image with header */}
            <Card title="CARD WITH DIVIDER">
                {
                users.map((u, i) => {
                    return (
                    <View key={i} style={styles.user}>
                        {/* <Image
                            style={styles.image}
                            resizeMode="cover"
                            source={{ uri: u.avatar }}
                        /> */}

                        <Avatar 
                            size="medium"
                            onPress={() => console.log("Works!")}
                            rounded
                            source={{
                                uri:  u.avatar,
                            }}
                        />

                        <Text style={styles.name}>{u.name}</Text>

                    </View>
                    );
                })
                }
            </Card>
            
            {/* // implemented without image without header, using ListItem component */}
            <Card containerStyle={{padding: 0}} >
            {
                users.map((u, i) => {
                    return (
                        <ListItem
                            key={i}
                            roundAvatar
                            title={u.name}
                            // roundAvatar={{ uri: u.avatar}}
                            leftAvatar={{ source: { uri: u.avatar} }}
                        />
                    );
                })
            }
            </Card>
                        
            {/* // implemented with Text and Button as children */}
            <Card
                title='HELLO WORLD'
                image={require('./images/reactnative.png')}>
                <Text style={{marginBottom: 10}}>
                The idea with React Native Elements is more about component structure than actual design.
                </Text>
                <Button
                icon={<Icon name='code' color='#ffffff' />}
                buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                title='VIEW NOW' />
            </Card>

        </View>
    );

    }
}