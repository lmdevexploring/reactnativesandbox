import React from 'react'
import { View, ScrollView } from 'react-native'
import { AvatarSandbox } from './AvatarSandbox'
import { BadgeSandbox } from './BadgeSandbox'
import { ButtonsSandbox } from './ButtonsSandbox'
import { CardSandbox } from './CardSandbox'

export default class NativeElements extends React.Component {

    render()
    {
        return(
            <ScrollView>
                <CardSandbox></CardSandbox>
                <ButtonsSandbox></ButtonsSandbox>
                <BadgeSandbox></BadgeSandbox>
                <AvatarSandbox></AvatarSandbox>
            </ScrollView>
        );
    }

}
