import React from 'react';
import { View, Platform } from 'react-native'
import { Avatar, Badge, Text } from 'react-native-elements'


export class BadgeSandbox extends React.Component {

render()
{

  return (
    <View style={{minHeight: 600, flex: 1, flexDirection: "column"}}>
      <Text h1>Badges</Text>
    
      
      <View style={{minHeight: 200, flex: 1, flexDirection: "row", justifyContent: "space-evenly"}} >
        {/* // Standard badge */}
        <Badge value="99+" status="error" textStyle={{color: "white"}} width={100} minHeight={50} style={{}} />
        <Badge value={<Text>My Custom Badge</Text>} />
      </View>
      <View style={{minHeight: 200, flex: 1, flexDirection: "column", justifyContent: "space-evenly"}} >

        {/* // Mini badge */}
        <Badge status="success" value="success" textStyle={{color: "white", fontSize: 30}} minHeight={30} />
        <Badge status="error" value="error" />
        <Badge status="primary" value="primary" />
        <Badge status="warning" value="warning"/>

      </View>
      <View style={{minHeight: 200, flex: 1, flexDirection: "column", margin: 20}} >
        {/* // Avatar with mini badge */}
        <View style={{alignSelf: 'flex-start' }}>
          <Avatar
            rounded
            source={{
              uri: 'https://randomuser.me/api/portraits/men/41.jpg',
            }}
            size="large"
          />

          <Badge
            rounded
            status="success" value="2"
            containerStyle={{ position: 'absolute', top: 0, right: 0 }}
            textStyle={{fontSize: 14}}
            badgeStyle={{minHeight: 20, minWidth: 20}}
          />

        </View>

        {/* // withBadge HOC */}

        {/* const BadgedIcon = withBadge(1)(Icon) */}
        {/* <BadgedIcon type="ionicon" name="ios-chatbubbles" /> */}

    </View>
    
    </View>
  );


    {/* // Using the decorator proposal */}
    // @connect(state => ({
    //   notifications: state.notifications,
    // }))
    // @withBadge(props => props.notifications.length)
    // export default class MyDecoratedIcon extends React.Component {
    //   render() {
    //     return (
    //       <Icon type="ionicon" name="md-cart" />
    //     );
    //   }
    // }

  }


}