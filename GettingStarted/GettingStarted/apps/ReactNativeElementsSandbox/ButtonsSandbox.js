import React from 'react';
import { View, Platform } from 'react-native'
import { Button, ButtonGroup, Text } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';

export class ButtonsSandbox extends React.Component {

    constructor () {
        super()
        this.state = {
            selectedIndex: 2
        }
        this.updateIndex = this.updateIndex.bind(this)
    }

    updateIndex (selectedIndex) {
        this.setState({selectedIndex})
      }

    render()
    {
        const buttons = ['Hello', 'World', 'Buttons']
        const { selectedIndex } = this.state

        return(
            
            <View>
            
                <Text h1>Buttons</Text>

                <View style={{height: 500, flex: 1, justifyContent: "space-evenly", padding: 30}}>

                    <Button
                        title="Solid Button"
                    />

                    <Button
                    title="Outline button"
                    type="outline"
                    />

                    <Button
                    title="Clear button"
                    type="clear"
                    />

                    <Button
                    icon={
                        <Icon
                        name="save"
                        size={15}
                        color="white"
                        />
                    }
                    title=" Save"
                    />

                    <Button
                    icon={{
                        name: "arrow-right",
                        size: 15,
                        color: "white"
                    }}
                    title="Button with icon object"
                    />

                    <Button
                    icon={
                        <Icon
                        name="arrow-right"
                        size={15}
                        color="white"
                        />
                    }
                    iconRight
                    title="Button with right icon"
                    />

                    <Button
                    title="Loading button"
                    loading
                    />

                    <ButtonGroup
                        onPress={this.updateIndex}
                        selectedIndex={selectedIndex}
                        buttons={buttons}
                        containerStyle={{height: 50, }}
                        
                        />

                </View>

            </View>

        );

    }
}