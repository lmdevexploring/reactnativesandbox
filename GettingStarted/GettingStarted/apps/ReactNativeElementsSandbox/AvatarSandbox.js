import React from 'react';
import { ActivityIndicator, View, ToastAndroid, Platform } from 'react-native';
import { Avatar, Text } from 'react-native-elements';


export class AvatarSandbox extends React.Component {



    constructor(props){
        super(props);
        this.state = { isLoading: true}
      }

      

    componentDidMount(){
        this.setState ({ isLoading: false});
    }

    render(){


        if(this.state.isLoading){
          return(
            <View style={{flex: 1, padding: 20, justifyContent: "center"}}>
              <ActivityIndicator size="large"/>
            </View>
          )
        }
    
        return(

          <View style={{height: 400}} >

          <Text h1>Avatar</Text>

          <View style={{flex: 1, flexDirection: 'row', justifyContent: "space-evenly", alignItems: "center"}}>
                {/* // Standard Avatar */}
                <Avatar 
                  size="xlarge"
                  onPress={() => console.log("Works!")}
                  rounded
                  source={{
                      uri:
                      'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                }}
                />
                {/* // Avatar with Title */}
                <Avatar rounded title="MD" size="xlarge"/>
          </View>
          <View style={{flex: 1, flexDirection: 'row', justifyContent: "space-evenly", alignItems: "center" }}>
              {/* // Avatar with Icon */}
              <Avatar size="xlarge" rounded icon={{ name: 'home' }} />
                {/* // Standard Avatar with edit button */}
                <Avatar
                size="xlarge"
                rounded
                source={{
                    uri:
                    'https://s3.amazonaws.com/uifaces/faces/twitter/rem/128.jpg',
                }}
                showEditButton
                onEditPress={() => 
                  {
                    if (Platform.OS === "android") 
                    {
                      ToastAndroid.showWithGravity("Pika, Pika, Pikachu!!!!", 
                      ToastAndroid.LONG, ToastAndroid.CENTER)
                    }
                    else {
                      console.log("Works!")
                    }
                  }
                }
                />   
            </View>
        </View>
        );
    }
}
    

