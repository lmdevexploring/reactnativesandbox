import React from 'react';
import { FlatList, ActivityIndicator, Text, View } from 'react-native';
import { Card, Header, Button } from 'react-native-elements'
// import Button from 'react-native-vector-icons/FontAwesome';

export default class UMinhoEspacos extends React.Component {

  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }


  componentDidMount(){

    try {
      let u = "https://campi.uminho.pt/api/maps/search/sala/1/50";
  
      return fetch(u)
        .then(response => response.json())
        .then((response) => {
          // console.log("response: " && response);
          // console.log('LISTA: ' && response.ListaEspacos); 
          this.setState(
            {
              dataSource: response.ListaEspacos,
              isLoading: false
            }
          )          

        })
        .catch((err) => {
            console.log('fetch', err)
        })
             
    } catch (error) {
      console.error(error);
    }
  }    


  render(){

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(

        <View style={{flex: 1, paddingTop:20}}>
          {/* <Header
            leftComponent={{ icon: 'menu', color: '#fff' }}
            centerComponent={{ text: 'Espaços', style: { color: '#fff' } }}
            rightComponent={{ icon: 'home', color: '#fff', type='FontAwesome' }} >
          </Header> */}
  
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) => 
            <Card>
                  <Button
                    icon={{name: 'envira', type: 'font-awesome'}}
                    title='BUTTON' />
              <Text>{item.EspacoId}</Text>
              <Text>{item.EspacoDesignacao}</Text>
              
            </Card>
            }
          keyExtractor={({EspacoId}, index) => EspacoId}
        />
      </View>
    );
  }

}