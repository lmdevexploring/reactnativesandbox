import React, { Component } from 'react';
import { View } from 'react-native';

export default class AlignItemsPosition extends Component {
  render() {
    return (

    <View style={{flexDirection: "row", alignSelf: "stretch", flex: 1, borderColor: "#aaa", borderWidth: 3 }}>

      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'flex-start', borderColor: "#aaa", borderWidth: 3,}}>
        <View style={{width: 50, height: 100, backgroundColor: 'red', position: "relative", top: 30, left: 30}} />
        <View style={{width: 50, height: 100, backgroundColor: 'orange', position: "relative", top: 60, left: 60}} />
        <View style={{width: 50, height: 100, backgroundColor: 'green', position: "relative", top: 90, left: 90}} />
      </View>

      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'flex-start', borderColor: "#aaa", borderWidth: 3, }}>
        <View style={{width: 50, height: 100, backgroundColor: 'red', position: "absolute", top: 30, left: 30}} />
        <View style={{width: 50, height: 100, backgroundColor: 'orange', position: "absolute", top: 60, left: 60}} />
        <View style={{width: 50, height: 100, backgroundColor: 'green', position: "absolute", top: 90, left: 90}} />
      </View>


    </View>        

    );
  }
};
