import React from 'react'
import { ScrollView, View } from 'react-native'
import { Header, Icon } from 'react-native-elements'
import { CheckboxSandbox } from './CheckboxSandbox'
import { InputSandbox } from './InputSandbox'

export default class NativeElements extends React.Component {

    render()
    {
        return(
            <View style={{flex:1}}>
                <Header
                    placement="left"
                    leftComponent={{ icon: 'menu', color: '#fff' }}
                    centerComponent={{ text: 'Second Test with native elements', style: { color: '#fff' } }}
                    rightComponent={<Icon name="home" color="white" onPress={() => {console.log("Home pressed!")}}></Icon> }
                    containerStyle={{ height: 60 }}
                    />
                <ScrollView>
                    <InputSandbox></InputSandbox>
                    <CheckboxSandbox></CheckboxSandbox>
                </ScrollView>
            </View>
        );
    }

}
