import React from 'react'
import { View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Input, Text } from 'react-native-elements'


export class InputSandbox extends React.Component
{
    render()
    {
        return (

            <View>

                <Text h1>Inputs</Text>

                <Input
                placeholder='BASIC INPUT'
                />
                
                <Input
                    placeholder='email@address.tld'
                    leftIcon={{ name: 'email' }}
                />
                
                <Input
                    placeholder='username'
                    leftIcon={
                    <Icon
                        name='user'
                        size={24}
                        color='black'
                    />
                    }
                />
            
                <Input
                    placeholder='this input has an error'
                    errorStyle={{ color: 'red' }}
                    errorMessage='this is its error'
                />     

            </View>   

       )

    };



}