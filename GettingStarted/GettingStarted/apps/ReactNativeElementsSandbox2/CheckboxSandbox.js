import React from 'react'
import { View } from 'react-native'
import { CheckBox, Text, Image, Divider } from 'react-native-elements'

export class CheckboxSandbox extends React.Component {

    constructor ()
    {
        super()
        this.state = {
            checked: false    
        }
    }

    componentDidMount()
    {
        

    }

    render()
    {
        return(
            <View>
                <Text h1>CheckBoxes</Text>

                <CheckBox
                    title='Click Here'
                    checked={this.state.checked}
                    uncheckedColor='red'
                    checkedColor='green'
                />

                <CheckBox
                    center
                    title='Click Here'
                    checked={this.state.checked}
                />

                <CheckBox
                    
                    title='Click Here'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.checked}
                />

                <CheckBox
                    
                    title='Click Here to Remove This Item'
                    iconRight
                    iconType='material'
                    checkedIcon='clear'
                    uncheckedIcon='add'
                    checkedColor='red'
                    checked={this.state.checked}
                />

                <Divider style={{ backgroundColor: 'blue' }} />

                <CheckBox
                    checkedTitle="uncheck everything"
                    title="check everything"
                    checkedIcon={<Image source={ require('./images/unchecked.png') } style={{width: 30, height: 30}} ></Image>}
                    uncheckedIcon={<Image source={ require('./images/checked.png') } style={{width: 30, height: 30}} ></Image>}
                    checked={this.state.checked}
                    onPress={() => this.setState({checked: !this.state.checked})}
                    // containerStyle={{marginTop: 30}}
                />


            </View>
        );
    }

}
