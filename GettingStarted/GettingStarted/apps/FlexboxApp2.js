import React, { Component } from 'react';
import { View } from 'react-native';

export default class AlignItemsBasics extends Component {
  render() {
    return (

    <View style={{flexDirection: "column", alignSelf: "stretch", flex: 1, borderColor: "#aaa", borderWidth: 3 }}>

      {/* Try setting `alignItems` to 'flex-start'
      Try setting `justifyContent` to `flex-end`.
      Try setting `flexDirection` to `row`. */}

      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'stretch', borderColor: "#aaa", borderWidth: 3}}>
        <View style={{width: 50, height: 50, backgroundColor: 'red'}} />
        <View style={{height: 35, backgroundColor: 'orange'}} />
        <View style={{height: 25, backgroundColor: 'green'}} />
      </View>

      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', borderColor: "#aaa", borderWidth: 3}}>
        <View style={{width: 50, height: 50, backgroundColor: 'red'}} />
        <View style={{width: 50, height: 50, backgroundColor: 'orange'}} />
        <View style={{width: 50, height: 50, backgroundColor: 'green'}} />
      </View>

      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', borderColor: "#aaa", borderWidth: 3}}>
        <View style={{width: 50, height: 50, backgroundColor: 'red', alignSelf: "flex-start"}} />
        <View style={{width: 50, height: 50, backgroundColor: 'orange', }} />
        <View style={{width: 50, height: 50, backgroundColor: 'green', alignSelf: "flex-end"}} />
      </View>

      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', borderColor: "#aaa", borderWidth: 3}}>
        <View style={{width: 50, height: 50, backgroundColor: 'red', alignSelf: "flex-start"}} />
        <View style={{height: 50, backgroundColor: 'orange', alignSelf: "stretch" }} />
        <View style={{width: 50, height: 50, backgroundColor: 'green', alignSelf: "flex-end"}} />
      </View>


    </View>        

    );
  }
};
