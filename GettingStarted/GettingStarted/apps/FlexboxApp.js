import React, { Component } from 'react';
import { View } from 'react-native';

export default class FlexDirectionBasics extends Component {
  render() {
    return (

        <View style={{flex: 1, borderColor: "#aaa", borderWidth: 3, }}>
            <View style={{flex: 1, flexDirection: 'row', borderColor: "#aaa", borderWidth: 3 }}>
                <View style={{flex: 1, flexDirection: 'row', borderColor: "#aaa", borderWidth: 3 }}>
                    <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                    <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                    <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                </View>
                <View style={{flex: 1, flexDirection: 'row-reverse', borderColor: "#aaa", borderWidth: 3 }}>
                    <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                    <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                    <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row', borderColor: "#aaa", borderWidth: 3 }}>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: "space-between", borderColor: "#aaa", borderWidth: 3 }}>
                    <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                    <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                    <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row', borderColor: "#aaa", borderWidth: 3 }}>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: "space-around", borderColor: "#aaa", borderWidth: 3 }}>
                    <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                    <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                    <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row', borderColor: "#aaa", borderWidth: 3 }}>
                <View style={{flex: 1, flexDirection: 'column', justifyContent: "flex-start", borderColor: "#aaa", borderWidth: 3 }}>   
                    <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                    <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                    <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                </View>
                <View style={{flex: 1, flexDirection: 'column-reverse', justifyContent: "flex-start", borderColor: "#aaa", borderWidth: 3 }}>
                    <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                    <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                    <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                </View>
            </View>
        </View>
    );
  }
};
