import React, { Component } from 'react';
import { View } from 'react-native';

export default class AlignItemsBasics extends Component {
  render() {
    return (

    <View style={{flexDirection: "row", alignSelf: "stretch", flex: 1, borderColor: "#aaa", borderWidth: 3 }}>

      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'center', borderColor: "#aaa", borderWidth: 3}}>
        <View style={{width: 50, height: 100, backgroundColor: 'red', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'orange', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'green', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'blue', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'lightblue', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'lightgreen', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'brown', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'black', }} />
      </View>

      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'center', borderColor: "#aaa", borderWidth: 3, flexWrap: "wrap", alignContent: "center"}}>
        <View style={{width: 50, height: 100, backgroundColor: 'red', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'orange', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'green', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'blue', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'lightblue', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'lightgreen', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'brown', }} />
        <View style={{width: 50, height: 100, backgroundColor: 'black', }} />
      </View>


    </View>        

    );
  }
};
